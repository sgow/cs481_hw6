﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CS481_HW6.OwlBot
{

    //owlbot api v4
    public class Owlbot
    {
        //Create a list of definitions, because we dont know what a definition is, we need to create a class for it also
        public List<Definitions> Definitions { get; set; }
        public String Word { get; set; }
        public String Pronunciation { get; set; }

    }
    //define what is in a definition
    //owlbot api v2+v4
    public class Definitions {
        public String Type { get; set; }
        public String Definition { get; set; }
        public String Example { get; set; }

        //v4 
        public String Image_url { get; set; }
        //v4
        public String Emoji { get; set; }

        //error message
        //public String message { get; set; }

    }

    public class Error
    {
        public String Message { get; set; }
    }
   
}
