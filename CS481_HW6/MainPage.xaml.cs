﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using CS481_HW6.OwlBot;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;


//using conectivity from xamarin.essentials
using Xamarin.Essentials;

namespace CS481_HW6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
       
        public static HttpClient jc;

        public MainPage()
        {
            //initialize http client
            jc = new HttpClient();
            //set a user agent string so the api service can easily block misbehaving clients
            jc.DefaultRequestHeaders.Add("User_Agent", "Its broke, Block it 1.0");
            //set an api token for authorization:  so the api service can know who to blame and how to contact them based on who registered for the token
            jc.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", "d7b3556a22fc8dbfc031e8da762f47f9feb1361c");


            InitializeComponent();

        }


        //handle OWLBOT APIV2  fetch a list of definitions
        private async void FindDictionaryV2(object sender, EventArgs e)
        {
            List<Definitions> lookupv2;

            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {

                await DisplayAlert("Disconnected", "Please Connect to the Internet", "OK");
                return;
            }
            else if (Word.Text !=null)
            {

                String jsonString ;
                //json api site
                String site = "https://owlbot.info/";
                //relative path
                String sitefunc = "api/v2/dictionary/";
                //specify json format in request
                String format = "?format=json";
                //format url string
                String url = site + sitefunc + Word.Text + format;
                Console.WriteLine("FETCHING : " + url);

                // create http request message pointing  to api resource using http get method
                HttpRequestMessage ReqUri = new HttpRequestMessage(HttpMethod.Get, url);

                // http request and wait async 
                HttpResponseMessage contentWrapper = await jc.SendAsync(ReqUri);

                //check if fetching was success using http
                if (contentWrapper.IsSuccessStatusCode)
                {
                    //FOR HTTP + JSON DEBUGING
                  //  Console.WriteLine("FETCHED : ");

                    //copy from http content to string
                    jsonString = await contentWrapper.Content.ReadAsStringAsync();
                    //DISPLAY RAW JSON
                    Console.WriteLine(jsonString);


                    //V2
                    if (jsonString != null)
                    {
                        try
                        {
                            //settings for json
                
                            JsonSerializerSettings settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Include, MissingMemberHandling = MissingMemberHandling.Error };

                            //convert json string to object
                            lookupv2 = JsonConvert.DeserializeObject<List<Definitions>>(jsonString, settings);

                            //add definitions to itemsource, null list results in the below
                            Definitions.ItemsSource = lookupv2;
                        }
                        //catch unexpected json message
                        catch (Exception je)
                        {
                            //debug statements
                            Console.WriteLine("ERROR : ");
                            Console.WriteLine(je.Message);

                            //parse special error message from json
                            List<Error> error = JsonConvert.DeserializeObject<List<Error>>(jsonString);
                            
                            await DisplayAlert("Error", error.ElementAt(0).Message, "Ok");
                        }
                    }
                }
                else if (contentWrapper.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    /*not found was returned using http status code */
                    //http status was not found, Fetch api status message from json error code anyways
                    try{
                        String errorJSON = await contentWrapper.Content.ReadAsStringAsync();
                        List<Error> error = JsonConvert.DeserializeObject<List<Error>>(errorJSON);
                        await DisplayAlert("Error", error.ElementAt(0).Message, "Ok");
                    }
                    catch(Exception je)
                    {
                        //display failsafe error message
                        Console.WriteLine(je.Message);
                        await DisplayAlert("Error", contentWrapper.ReasonPhrase, "Ok");
                    }
                }
            }
            else
            {
                await DisplayAlert("No Word", "Enter a Word", "OK");
                return;
            }

        }


        //handle owlbot v4 api requests, fetch an object with a list of definitions
            private async void FindDictionaryV4(object sender, EventArgs e)
        {
            Owlbot lookupv4 ;

            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {

                await DisplayAlert("Disconnected", "Please Connect to the Internet", "OK");
                return;
            }
            else if (Word.Text!=null)         
            {


                String jsonString ;
                //json api site
                String site = "https://owlbot.info/";
                //relative path
                String sitefunc = "api/v4/dictionary/";
                //specify json format in request
                String format = "?format=json";
                //format url string
                String url = site + sitefunc + Word.Text + format;
                Console.WriteLine("FETCHING : " + url);

                // create http request message pointing  to api resource using http get method
                HttpRequestMessage ReqUri = new HttpRequestMessage(HttpMethod.Get, url);

                // http request and wait async 
                
                HttpResponseMessage contentWrapper = await jc.SendAsync(ReqUri);

                //check if fetching was success
                if (contentWrapper.IsSuccessStatusCode)
                {
                    //FOR HTTP + JSON DEBUGING
                    Console.WriteLine("FETCHED : ");

                    //copy from http content to string
                    jsonString = await contentWrapper.Content.ReadAsStringAsync();
                    //DISPLAY RAW JSON
                    Console.WriteLine(jsonString);


                    //V4
                    if (jsonString != null)
                    {
                        try
                        {
                            //settings for json
                            // ignore null values, ignored here, handled elsewhere
                            JsonSerializerSettings settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Include, MissingMemberHandling = MissingMemberHandling.Error };

                            //convert json string to object
                            lookupv4 = JsonConvert.DeserializeObject<Owlbot>(jsonString, settings);

                            //add definitions to itemsource, null list results in the below
                            Definitions.ItemsSource = lookupv4.Definitions;
                        }
                        //catch null error on definitions=null
                        catch (ArgumentNullException je)
                        {

                            Console.WriteLine("ERROR : ");
                            Console.WriteLine(je.Message);
                            await DisplayAlert("No Definition", "Check Spelling", "Ok");
                        }
                    }
                }
                else if (contentWrapper.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    //handle non success errers with server provided reasons

                    await DisplayAlert("Definition", contentWrapper.ReasonPhrase, "OK");
                }
            }
            else
            {
                await DisplayAlert("No Word", "Enter a Word", "OK");
                return;
            }
        }
    }
 }
    
















/*
 * Implement the JSON "https://owlbot.info/api/v2/dictionary/" = 10 pts  <<returns 404??
 done

Display the type of word = 1 pts
done

Display the definition of the word = 1 pts
done

Display an example of the word = 1 pts
done

Detect internet/wifi connection = 7 points
done

Error handling for bad/null JSON result and for no internet connection = 5 pts
done

Comments that explain what your code does = 20 points
done

At least five commits with actual code implementation, across at least 5 different days = 1 pt each


Miscellaneous Extras = up to +10 points
*/
  