﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Xamarin.Essentials;


namespace CS481_HW6
{
    public partial class App : Application
    {

        public App()
        {
        
            InitializeComponent();

            MainPage = new MainPage();
        }


    

        protected override void OnStart()
        {
            
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
